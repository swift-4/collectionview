//
//  ViewController.swift
//  CollectionView
//
//  Created by Yakup Caglan on 9.11.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource{

    
    var resimler = ["1","2","3","4","5"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resimler.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
        UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! CustomCell
            
            cell.layer.cornerRadius = 50
            cell.layer.borderColor = UIColor.gray.cgColor
            cell.layer.borderWidth = 3
            cell.resim.image = UIImage(named:resimler[indexPath.row])
            return cell
    }

   
}

