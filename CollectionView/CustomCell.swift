//
//  CustomCell.swift
//  CollectionView
//
//  Created by Yakup Caglan on 9.11.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell {
    
    @IBOutlet weak var resim: UIImageView!
}
